package chatrmi;

import java.io.Serializable;
import javax.swing.JOptionPane;

public class User implements Serializable {
	private static final long serialVersionUID = 1L;
	private String login;
	 
	public User(String login) {
		setLogin(login);
	}
	
	public String getLogin() {
		return login;
	}	
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public void exibir(User emissor, String mensagem) {
		JOptionPane.showMessageDialog(null, "Mensagem de " + emissor.getLogin() + " para: " + getLogin() + ": " + mensagem);
	}
	
	@Override
	public boolean equals(Object obj) {
		return getLogin().equals(((User)obj).getLogin());
	}	
}