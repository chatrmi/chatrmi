package chatrmi.server;

import java.rmi.RemoteException;
import java.util.Vector;

import chatrmi.ServicoChat;
import chatrmi.User;

public class ServicoChatImpl implements ServicoChat {
	private Vector<User> conectados;
	
	public ServicoChatImpl() {
		conectados = new Vector<User>();
	}
	
	@Override 
	public void conectar(User u) throws RemoteException {
		conectados.add(u);
	}
	
	@Override
	public void desconectar(User u) throws RemoteException {
		conectados.remove(u);
	}
	
	@Override
	public int getConectados() {
		return conectados.size();
	}
	
	@Override
	public void enviar(User userRemetente, String mensagem) throws RemoteException {
		for (User u : conectados) {			
			if(!u.equals(userRemetente)) {
				u.exibir(userRemetente, mensagem);
			}			
		}
	}
	
}