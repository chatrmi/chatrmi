
package chatrmi.server;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import chatrmi.ServicoChat;

public class ServicoChatRegistrator {
	 public static void main(String[] args) {
		 if (System.getSecurityManager() == null) {
			 System.setSecurityManager(new SecurityManager());
		 }
		 
		 try { 
			 ServicoChat servidorChat = new ServicoChatImpl();
			 ServicoChat stub = (ServicoChat) UnicastRemoteObject.exportObject(servidorChat);
			 
			 Registry registry = LocateRegistry.createRegistry(4000);
			 		  registry.rebind("ServicoChatOnline", stub);
			 
			 System.out.println("Servico de CHAT registrado com sucesso!");
		 } catch (Exception e) {
			 System.out.println("Erro ao registrar Servico de CHAT: ");
			 e.printStackTrace();
		 }
	 }
}