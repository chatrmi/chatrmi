package chatrmi.cliente;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.swing.JOptionPane;

import chatrmi.ServicoChat;
import chatrmi.User;

public class ServicoChatCliente extends Thread{
	
	public static void run(User usuario, String ip, int quantidade){		
		ServicoChat servico = returnConnection(usuario, ip, 0);
		
		if(servico == null){
			JOptionPane.showMessageDialog(null, "N�o foi poss�vel conectar ao servidor.");
			return;
		}
		
		try {
			comunicar(servico, usuario);
		} catch (RemoteException e) {
			run(usuario, ip, quantidade);
			e.printStackTrace();
		}		
	}
	
	public static ServicoChat returnConnection(User usuario, String ip, int quantidade){
		Registry registry;		
		ServicoChatCliente cliente = new ServicoChatCliente();
		
		try{
			if(quantidade == 5) return null;
			
			registry = LocateRegistry.getRegistry(ip, 4000);
			ServicoChat servChat = (ServicoChat) registry.lookup("ServicoChatOnline");
			servChat.conectar(usuario);
			
			return servChat;
		} catch (RemoteException e) {
			try {
				System.out.println("entrou");
				cliente.sleep(5000);
				System.out.println("saiu");
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
			
			return returnConnection(usuario, ip, quantidade + 1);
		} catch (NotBoundException e){
			e.printStackTrace();
		}

		return null;	
	}
	
	public static void comunicar(ServicoChat servChat, User usuario) throws RemoteException{
		String mensagem = null;	
		boolean flag = true;
		
		do { 
			if(servChat.getConectados() == 1){
				if(flag == true) JOptionPane.showMessageDialog(null, "N�o existe nenhum usu�rio conectado.");
				
				flag = false;
				continue;
			} else
				flag = true;
			
			mensagem = JOptionPane.showInputDialog("Informar mensagem, ou escreva Sair para desconectar");
			
			if(mensagem.equalsIgnoreCase("sair")) break;
			
			servChat.enviar(usuario, mensagem);
		} while (mensagem == null || !mensagem.equalsIgnoreCase("sair"));
		
		servChat.desconectar(usuario);
	}
		
	public static void main(String[] args) {
		String ip = JOptionPane.showInputDialog("informar IP do servidor:");
	   		   ip = (ip == null || ip.equals("")) ? "localhost" : ip;
	   	   
	   	String login = JOptionPane.showInputDialog("informar seu login");			
		User usuario = new User(login);
		run(usuario, ip, 0);
	}
}
