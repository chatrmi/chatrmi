package chatrmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ServicoChat extends Remote {
	public void conectar(User u) throws RemoteException;
	public void enviar(User u, String mensagem) throws RemoteException;
	public void desconectar(User u) throws RemoteException;
	public int getConectados() throws RemoteException;
}